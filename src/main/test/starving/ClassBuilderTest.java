package starving;

import org.junit.Test;

import eu.matejkormuth.mojos.starving.generators.ClassBuilder;
import eu.matejkormuth.mojos.starving.generators.ClassBuilder.Class;
import eu.matejkormuth.mojos.starving.generators.ClassBuilder.Method;
import junit.framework.TestCase;

public class ClassBuilderTest extends TestCase {
    @Test
    public void test1() {
        ClassBuilder cb = new ClassBuilder("eu.matejkormuth", "UltraClass");
        cb.addImport("eu.matejkormuth.AnotherClass2");
        Class c = cb.openClass();
        c.addComment("This is class comment on first line.");
        c.addConstant("int", "constatnt_invalid_name", "18");
        c.addFieldInitialization("public", "String", "initializedString", "\"value\"");
        c.addFinalField("private", "int", "privateFinalField");
        c.addPublicNoArgConstructor();
        c.addPublicConstructor().closeMethod();
        c.addProperty("Double", "doubleProperty");
        c.addProtectedMethod("protectedVoidMethod").closeMethod();
        c.addPublicMethod("publicVoidMethod").closeMethod();
        Method m = c.addProtectedMethod("Object", "protectedReturnObject");
        m.addComment("Method comment");
        m.addStatement("int abc = 23");
        m.addReturn("abc");
        m.closeMethod();
        c.closeClass();
        
        System.out.println(cb.toString());
    }
}
