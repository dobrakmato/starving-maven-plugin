package eu.matejkormuth.mojos.starving;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import eu.matejkormuth.mojos.starving.generators.AtmospheresClassGenerator;
import eu.matejkormuth.mojos.starving.generators.SoundsClassGenerator;

@Mojo(name = "starving")
public class StarvingMojo extends AbstractMojo {
    @Parameter
    private File soundsIn;
    @Parameter
    private File atmospheresIn;
    @Parameter
    private File soundsOut;
    @Parameter
    private File atmospheresOut;

    public void execute() throws MojoExecutionException {
        if (this.soundsIn == null || this.soundsOut == null
                || this.atmospheresIn == null
                || this.atmospheresOut == null) {
            throw new MojoExecutionException("At least one of files is null!");
        }

        this.getLog().info(
                "Generating Sounds class from file "
                        + this.soundsIn.getAbsolutePath());
        try {
            List<String> lines = Files.readAllLines(soundsIn.toPath(),
                    Charset.defaultCharset());
            SoundsClassGenerator generator = new SoundsClassGenerator();
            generator.addPrivateConstructor();
            for (String line : lines) {
                String[] parts = line.split(Pattern.quote(","));
                generator.addConstant("NamedSound", parts[0].toUpperCase(),
                        "new NamedSound(\"" + parts[0] + "\", " + parts[1]
                                + ")");
            }
            generator.closeClass();
            generator.toFile(this.soundsOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getLog().info(
                "Generating Atmospheres class from file "
                        + this.atmospheresIn.getAbsolutePath());
        try {
            List<String> lines = Files.readAllLines(atmospheresIn.toPath(),
                    Charset.defaultCharset());
            AtmospheresClassGenerator generator = new AtmospheresClassGenerator();
            generator.addPrivateConstructor();
            for (String line : lines) {
                String[] parts = line.split(Pattern.quote(":"));
                String snds = "";
                for (String snd : parts[1].split(Pattern.quote(","))) {
                    snds += "Sounds." + snd.toUpperCase() + ", ";
                }
                generator.addConstant(
                        "Atmosphere",
                        parts[0].toUpperCase(),
                        "new Atmosphere("
                                + snds.substring(0, snds.length() - 2) + ")");
            }
            generator.closeClass();
            generator.toFile(this.atmospheresOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
