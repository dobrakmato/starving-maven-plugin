package eu.matejkormuth.mojos.starving.generators;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ClassBuilder {
    private final static char SEMICOLON = ';';
    private final static char SPACE = ' ';
    private final static char EQUALS = '=';
    private final static String IDENTATION = "    ";
    private final static String VOID = "void";

    private StringBuilder contents;
    private String classPackage;
    private String className;
    private int ident = 0;

    public ClassBuilder(String classPackage, String className) {
        this.contents = new StringBuilder();
        this.className = className;
        this.classPackage = classPackage;

        // Add package declaration.
        line(this.ident, "package " + this.classPackage + ";\n");
    }

    protected void line(int i, String cont) {
        this.contents.append(new String(new char[i * 4]) + cont + "\n");
    }

    public void addImport(String canonicalName) {
        line(this.ident, "import " + canonicalName + SEMICOLON);
    }

    public Class openClass() {
        return openClass("public");
    }

    public Class openClass(String modifiers) {
        line(this.ident, "\n" + modifiers + " class " + this.className + " {");
        return new Class(ident + 1);
    }

    public void addComment(String comment) {
        line(this.ident, "// " + comment);
    }

    public void toFile(File file) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(this.contents.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    @Override
    public String toString() {
        return this.contents.toString();
    }

    public class Class {
        private int ident;

        private Class(int ident) {
            this.ident = ident;
        }

        public void addPrivateField(String type, String name) {
            this.addField("private", type, name);
        }

        public void addPublicField(String type, String name) {
            this.addField("public", type, name);
        }

        public void addField(String modifiers, String type, String name) {
            line(this.ident, modifiers + SPACE + type + SPACE + name
                    + SEMICOLON);
        }

        public void addFieldInitialization(String modifiers, String type,
                String name, String value) {
            line(this.ident, modifiers + SPACE + type + SPACE + name + SPACE
                    + EQUALS + SPACE + value + SEMICOLON);
        }

        public void addFinalField(String modifiers, String type, String name) {
            line(this.ident, "final " + modifiers + SPACE + type + SPACE + name
                    + SEMICOLON);
        }

        public void addProperty(String type, String name) {
            addField("private", type, name);
            addGetter(type, name);
            addSetter(type, name);
        }

        public void addGetter(String type, String fieldName) {
            line(this.ident,
                    "public " + type + " get"
                            + fieldName.substring(0, 1).toUpperCase()
                            + fieldName.substring(1) + "() {");
            line(this.ident, IDENTATION + "return this." + fieldName + SEMICOLON);
            line(this.ident, "}\n");
        }

        public void addSetter(String type, String fieldName) {
            line(this.ident,
                    "public " + type + " set"
                            + fieldName.substring(0, 1).toUpperCase()
                            + fieldName.substring(1) + "(" + type + SPACE
                            + fieldName + ") {");
            line(this.ident, IDENTATION + "this." + fieldName + SPACE + EQUALS
                    + SPACE + fieldName + SEMICOLON);
            line(this.ident, "}\n");
        }

        public void addPrivateNoArgConstructor() {
            line(this.ident, "private " + className + "() {\n" + new String(new char[this.ident * 4]) + "}\n");
        }

        public void addPublicNoArgConstructor() {
            line(this.ident, "public " + className + "() {\n" + new String(new char[this.ident * 4]) + "}\n");
        }

        public Method addPrivateConstructor() {
            return this.addPrivateConstructor("");
        }

        public Method addPrivateConstructor(String args) {
            line(this.ident, "private " + className + "(" + args + ") {");
            return new Method(this.ident + 1);
        }

        public Method addProtectedConstructor() {
            return this.addPrivateConstructor("");
        }

        public Method addProtectedConstructor(String args) {
            line(this.ident, "protected " + className + "(" + args + ") {");
            return new Method(this.ident + 1);
        }

        public Method addPublicConstructor() {
            return this.addPrivateConstructor("");
        }

        public Method addPublicConstructor(String args) {
            line(this.ident, "public " + className + "(" + args + ") {");
            return new Method(this.ident + 1);
        }

        public void addConstant(String type, String name, String value) {
            line(this.ident, "public static final " + type + " " + name + " = "
                    + value + SEMICOLON);
        }

        public Method addProtectedMethod(String name) {
            return this.addMethod("protected", VOID, name, "");
        }

        public Method addProtectedMethod(String returnType, String name) {
            return this.addMethod("protected", returnType, name, "");
        }

        public Method addProtectedMethod(String returnType, String name,
                String args) {
            return this.addMethod("protected", returnType, name, args);
        }

        public Method addPrivateMethod(String name) {
            return this.addMethod("private", VOID, name, "");
        }

        public Method addPrivateMethod(String returnType, String name) {
            return this.addMethod("private", returnType, name, "");
        }

        public Method addPrivateMethod(String returnType, String name,
                String args) {
            return this.addMethod("private", returnType, name, args);
        }

        public Method addPublicMethod(String name) {
            return this.addMethod("public", VOID, name, "");
        }

        public Method addPublicMethod(String returnType, String name) {
            return this.addMethod("public", returnType, name, "");
        }

        public Method addPublicMethod(String returnType, String name,
                String args) {
            return this.addMethod("public", returnType, name, args);
        }

        public Method addPublicStaticMethod(String returnType, String name,
                String args) {
            return this.addMethod("public static", returnType, name, args);
        }

        public Method addPublicStaticMethod(String returnType, String name) {
            return this.addMethod("public static", returnType, name, "");
        }

        public Method addMethod(String modifiers, String returnType,
                String name, String args) {
            line(this.ident, modifiers + SPACE + returnType + SPACE + name
                    + "(" + args + ") {");
            return new Method(this.ident + 1);
        }

        public Method addMethod(String modifiers, String name, String args) {
            line(this.ident, modifiers + SPACE + VOID + SPACE + name + "("
                    + args + ") {");
            return new Method(this.ident + 1);
        }

        public void addComment(String comment) {
            line(this.ident, "// " + comment);
        }

        public void closeClass() {
            line(this.ident - 1, "}\n");
        }
    }

    public class Method {
        private int ident;

        private Method(int ident) {
            this.ident = ident;
        }

        public void addComment(String comment) {
            line(this.ident, "// " + comment);
        }

        public void addStatement(String statement) {
            line(this.ident, statement + SEMICOLON);
        }

        public void addReturn(String objectName) {
            line(this.ident, "return " + objectName + SEMICOLON);
        }

        public void closeMethod() {
            line(this.ident - 1, "}\n");
        }
    }
}
