package eu.matejkormuth.mojos.starving.generators;

public class AtmospheresClassGenerator extends ClassGenerator {

    public AtmospheresClassGenerator() {
        super("eu.matejkormuth.rpgdavid.starving.sounds.lists", "Atmospheres");

        this.addHeader();
        this.addLicense();
        this.addPackage();
        this.addImport("eu.matejkormuth.rpgdavid.starving.sounds.Atmosphere");
        this.openClass("public");
    }

}
