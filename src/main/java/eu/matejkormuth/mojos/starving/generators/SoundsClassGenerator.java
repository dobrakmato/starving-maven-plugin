package eu.matejkormuth.mojos.starving.generators;

public class SoundsClassGenerator extends ClassGenerator {
    public SoundsClassGenerator() {
        super("eu.matejkormuth.rpgdavid.starving.sounds.lists", "Sounds");

        this.addHeader();
        this.addLicense();
        this.addPackage();
        this.addImport("eu.matejkormuth.rpgdavid.starving.sounds.NamedSound");
        this.openClass("public");
    }
}
